Security related utility library

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/CRYPTO>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
