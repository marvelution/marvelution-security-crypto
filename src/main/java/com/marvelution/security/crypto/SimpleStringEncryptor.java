/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.security.crypto;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * Simple {@link StringEncryptor} implementation to encrypt and decrypt strings using the DESede scheme
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class SimpleStringEncryptor implements StringEncryptor {

	private static final String UNICODE_FORMAT = "UTF8";
	private static final String DESEDE_ENCRYPTION_SCHEME = "DESede";

	private Cipher cipher;
	private SecretKeyFactory keyFactory;
	private DESedeKeySpec keySpec;

	/**
	 * Constructor
	 *
	 * @param password the Password used to initiate the {@link DESedeKeySpec}
	 */
	public SimpleStringEncryptor(String password) {
		if (password == null || password.length() < 25) {
			throw new IllegalArgumentException("The password must not be null and at least 25 character long");
		}
		try {
			keySpec = new DESedeKeySpec(password.getBytes(UNICODE_FORMAT));
			keyFactory = SecretKeyFactory.getInstance(DESEDE_ENCRYPTION_SCHEME);
			cipher = Cipher.getInstance(DESEDE_ENCRYPTION_SCHEME);
		} catch (InvalidKeyException e) {
			throw new IllegalArgumentException("Unable to initialize due to invalid secret key", e);
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException("UTF-8 is not an available char set", e);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalArgumentException("Not a valid encryption algorithm", e);
		} catch (NoSuchPaddingException e) {
			throw new IllegalStateException("Should not happen", e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String encrypt(String string) {
		if (string == null || string.isEmpty()) {
			return "";
		}
		synchronized (cipher) {
			try {
				initializeCipher(Cipher.ENCRYPT_MODE);
				return new String(Base64.encodeBase64(cipher.doFinal(string.getBytes(UNICODE_FORMAT))));
			} catch (InvalidKeyException e) {
				throw new IllegalArgumentException("Unable to initialize due to invalid secret key", e);
			} catch (InvalidKeySpecException e) {
				throw new IllegalArgumentException("Not a valid secert key", e);
			} catch (IllegalBlockSizeException e) {
				throw new IllegalStateException("Unable to invoke Cipher due to illegal block size", e);
			} catch (BadPaddingException e) {
				throw new IllegalStateException("Unable to invoke Cipher due to bad padding", e);
			} catch (UnsupportedEncodingException e) {
				throw new IllegalStateException("UTF-8 is not an available char set", e);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String decrypt(String encryptedString) {
		if (encryptedString == null || encryptedString.isEmpty()) {
			return "";
		}
		synchronized (cipher) {
			try {
				initializeCipher(Cipher.DECRYPT_MODE);
				return new String(cipher.doFinal(Base64.decodeBase64(encryptedString.getBytes())));
			} catch (InvalidKeyException e) {
				throw new IllegalArgumentException("Unable to initialize due to invalid secret key", e);
			} catch (InvalidKeySpecException e) {
				throw new IllegalArgumentException("Not a valid secert key", e);
			} catch (IllegalBlockSizeException e) {
				throw new IllegalStateException("Unable to invoke Cipher due to illegal block size", e);
			} catch (BadPaddingException e) {
				throw new IllegalStateException("Unable to invoke Cipher due to bad padding", e);
			}
		}
	}

	/**
	 * Initialize the {@link Cipher} implementation
	 * 
	 * @param mode the {@link Cipher} operation mode
	 * @throws InvalidKeySpecException in case of {@link SecretKey} generation exception
	 * @throws InvalidKeyException in case the {@link Cipher} cannot be initialized
	 */
	private void initializeCipher(int mode) throws InvalidKeySpecException, InvalidKeyException {
		final SecretKey secretKey = keyFactory.generateSecret(keySpec);
		cipher.init(mode, secretKey);
	}

}
