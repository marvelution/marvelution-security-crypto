/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.security.crypto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Testcase for the {@link SimpleStringEncryptor}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class SimpleStringEncryptorTest {

	StringEncryptor encryptor;

	/**
	 * Setup the {@link SimpleStringEncryptor} for the tests
	 * 
	 * @throws Exception in case of instanciation errors
	 */
	@Before
	public void setup() throws Exception {
		encryptor = new SimpleStringEncryptor("123456789012345678901234567890");
	}

	/**
	 * Reset the {@link SimpleStringEncryptor} for the next test
	 * 
	 * @throws Exception in case of errors
	 */
	@After
	public void taerdown() throws Exception {
		encryptor = null;
	}

	/**
	 * Test illegal password length
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testIllegalInstanciation() throws Exception {
		try {
			new SimpleStringEncryptor("1234567890");
			fail("This test should fail as the password needs to be atleast 25 characters");
		} catch (Exception e) {
			assertEquals(IllegalArgumentException.class, e.getClass());
		}
	}

	/**
	 * Test {@link StringEncryptor#encrypt(String)}
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testEncrypt() throws Exception {
		assertEquals("05vjx6GagmFH/qMvT8yIMQ==", encryptor.encrypt("encryption test"));
	}

	/**
	 * Test {@link StringEncryptor#decrypt(String)}
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDecrypt() throws Exception {
		assertEquals("encryption test", encryptor.decrypt("05vjx6GagmFH/qMvT8yIMQ=="));
	}

	/**
	 * Test {@link StringEncryptor#decrypt(String)} a not encrypted string
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDecryptInvalidString() throws Exception {
		try {
			encryptor.decrypt("encryption test");
			fail("This test should fail as the encrypted string is not correct");
		} catch (Exception e) {
			assertEquals(IllegalStateException.class, e.getClass());
		}
	}

}
